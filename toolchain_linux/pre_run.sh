#!/bin/bash

#####################################################################################
#  Copyright 2019 Lawinenwarndienst Tirol / Avalanche Warning Service Tyrol         #
#####################################################################################
#  This is free software: you can redistribute and/or modify it under the terms of  #
#  the GNU Lesser General Public License 3 or later: http://www.gnu.org/licenses    #
#####################################################################################

#Called before and meteo filtering and processing is performed.
#Michael Reisecker, 2019-01

#clear the meteo output path so that we start tidy:
echo "- Clearing old output..."
rm -f "$outfolder_meteo"/*
