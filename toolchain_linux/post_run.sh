#!/bin/bash

#####################################################################################
#  Copyright 2019 Lawinenwarndienst Tirol / Avalanche Warning Service Tyrol         #
#####################################################################################
#  This is free software: you can redistribute and/or modify it under the terms of  #
#  the GNU Lesser General Public License 3 or later: http://www.gnu.org/licenses    #
#####################################################################################

#Called after the meteo data filtering and processing have fully taken place.
#This is only for testing purposes, since the real program will run on Windows.
#Michael Reisecker, 2019-01

if [ "$convert_to_latin" = true ]
then
	echo "- Converting to IOS-Latin-1..."
	for file in "$outfolder_meteo"/*
	do
		iconv -f UTF-8 -t ISO-8859-1 "$file" -o "$file".part
		mv -f "$file".part "$file"
	done
fi

if [ "$perform_data_qa" = true ]
then
	echo "- Preparing quality report..."
	cd "$main_script_path"
	cd "$data_qa_path"
	./data_qa.sh qa.log #should be fetched from the ini
fi

if [ "$plot_results_in_r" = true ]
then
	echo "- Invoking R-Script"
	cd "$main_script_path"
	Rscript ./plot_stations.r
fi

echo "- All done"
