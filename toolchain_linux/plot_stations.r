#!/usr/lib/R/bin/Rscript

#####################################################################################
#  Copyright 2019 Lawinenwarndienst Tirol / Avalanche Warning Service Tyrol         #
#####################################################################################
#  This is free software: you can redistribute and/or modify it under the terms of  #
#  the GNU Lesser General Public License 3 or later: http://www.gnu.org/licenses    #
#####################################################################################

#Simple plots of all stations prepared by MeteoIO, i. e. all files to view results.
#This uses the RSMET package; check the Saalbach project for a custom solution.
#Michael Reisecker, 2019-11

print("+ This is plot_smet.r")

print("+ Loading package")
suppressMessages(require("RSMET"))

files.infolder <- '../output/'
files.outfolder <- '../plot/'
files.infiles <- list.files(files.infolder)

for (f in 1:length(files.infiles)) {

	station.file <- files.infiles[f]
	station.name <- tools::file_path_sans_ext(station.file)
	station.inpath <- paste(files.infolder, station.file, sep = "")
	station.outpath <- paste(files.outfolder, station.name, ".pdf", sep = "")

	print(paste("+ Reading data for station", station.name, "...", sep = " "))
	smet <- as.data.frame(smet(station.inpath))

	plot_colors <- strsplit(gsub('0x', '#', attr(smet, "header")$plot_color), " ") #split into to proper hex colors
	plot_colors <- plot_colors[[1]] #unlist

	print("+ Plotting all data for this station...")
	pdf(file = station.outpath, paper = "A4r", width = 15, height = 10) #landscape A4
	
	#some semi-automatic per parameter settings
	for (p in 2:dim(smet)[2]) { #plot all parameters
		varname <- colnames(smet)[p]
		plot_type = "l"
		
		if (length(smet[,p][!is.na(smet[,p])]) == 0) {
			print(paste("+++ Empty data for parameter", varname), " ")
			next	
		}

		if (varname == "TA" || varname == "TSS" || varname == "TSG") {
			smet[,p] = smet[,p]-273.15
		} else if (varname == "DW") {
			plot_type = "p"
		} else if (varname == "null") {
			print("+ There's a variable that should be renamed")
		}

		plot(smet$timestamp[!is.na(smet[,p])], na.omit(smet[,p]),
		    xlab = "", ylab = varname, xaxt = "n",
		    type = plot_type,
		    col = plot_colors[p],
		    main = station.name,
		)

		#choose tick mark locations depending on the data window (# of days):
		dur <- (as.numeric(smet$timestamp[length(smet$timestamp)]) - as.numeric(smet$timestamp[1])) / (3600*24)
		if (dur <= 31) {
			tickskip = "days"
		} else if (dur <= 62) {
			tickskip = "2 days"
		} else if (dur <= 365) {
			tickskip = "weeks"
		} else {
			tickskip = "months"
		}
		
		#this is just to display rotated tick marks and a grid:
		x_ticks <- seq(smet$timestamp[1], smet$timestamp[length(smet$timestamp)], by = tickskip)
		ticks <- axis.POSIXct(1, at = as.POSIXct(x_ticks), labels = F)
		abline(v = ticks, col = "grey80") #vertical grid at tick locations
		grid(NA, NULL) #horizontal grid 
		margins <- par("usr") #(xmin, xmax, ymin, ymax)
		offset = (margins[4] - margins[3]) * 0.03
		text(x = x_ticks, y = margins[3] - offset, labels = format(x_ticks, "%Y-%m-%d"), srt = 45, adj = 1, xpd = T)

		} #endif varname		
	} #end for p

print("+ Done")
