#####################################################################################
#  Copyright 2019 Lawinenwarndienst Tirol / Avalanche Warning Service Tyrol         #
#####################################################################################
#  This is free software: you can redistribute and/or modify it under the terms of  #
#  the GNU Lesser General Public License 3 or later: http://www.gnu.org/licenses    #
#####################################################################################

#This makefile compiles the MeteoIO control program.
#Michael Reisecker, 2019-02

CC      = g++
CFLAGS  = -Wall -Wextra
DEBUG   = -g
#DEBUG  += -Wno-psabi #Wno-psabi silences "... will change in gcc version..."
METEOIODIR = /usr/local/include

LIBS    = -rdynamic -lstdc++ -lmeteoio -ldl
INCLUDE = -I. -I$(METEOIODIR)

#####################
#	RULES
#####################
.cc.o: $*.cc $*.h
	$(CC) $(DEBUG) $(CFLAGS) -c $< $(INCLUDE)

%.o: %.cc
	$(CC) $(DEBUG) $(CFLAGS) -c $< $(INCLUDE)

#####################
#	TARGETS
#####################
all: processor

processor: processor.o
	$(CC) $(DEBUG) -o $@ $@.o ${LIBS}

clean:
	rm -rf *~ *.o processor